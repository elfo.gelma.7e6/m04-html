<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">   
    <xsl:template match="/mundo">
    <html>
        <head>
            <link rel="stylesheet" href="mundo.css"/>
        </head>
        <body>
            <xsl:apply-templates select="continente"/>
        </body>
    </html>

    </xsl:template>
    <xsl:template match="continente">
        <xsl:element name="h2">
                <xsl:value-of select="nombre"/>
            </xsl:element>
        <xsl:element name="table">
            <xsl:attribute name="class"><xsl:value-of select="nombre/@color"/></xsl:attribute>
            <tr>
                <th>Bandera</th>
                <th>País</th>
                <th>Gobierno</th>
                <th>Capital</th>
            </tr>
            <xsl:apply-templates select="paises"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="paises">
        <xsl:for-each select="pais">
            <xsl:sort select="nombre" data-type="text" order="ascending"/>
            <tr>
                <th>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            img/<xsl:value-of select="foto"/>
                        </xsl:attribute>
                    </xsl:element>
                </th>
                <th><xsl:value-of select="nombre"/></th>
                <xsl:choose>
                    <xsl:when test="nombre/@gobierno ='monarquia'">
                        <th class="monarquia"><xsl:value-of select="nombre/@gobierno"/></th>
                    </xsl:when>
                    <xsl:when test="nombre/@gobierno ='dictadura'">
                        <th class="dictadura"><xsl:value-of select="nombre/@gobierno"/></th>
                    </xsl:when>
                    <xsl:otherwise>
                        <th><xsl:value-of select="nombre/@gobierno"/></th>
                    </xsl:otherwise>
                </xsl:choose>
                <th><xsl:value-of select="capital"/></th>
            </tr>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>