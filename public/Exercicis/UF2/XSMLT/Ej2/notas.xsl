<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/evaluacion">
        <html>
            <body>
                <h1>M04 Notas</h1>
                <table style="border: 1px solid blue">
                    <tr style="background-color:lightblue">
                        <th style="text-align:left">Nombre</th>
                        <th style="text-align:left">Apellidos</th>
                        <th style="text-align:left">Telefono</th>
                        <th style="text-align:left">Repetidor</th>
                        <th style="text-align:left">Nota Practica</th>
                        <th style="text-align:left">Nota Examen</th>
                        <th style="text-align:left">Nota Total</th>
                        <th>Imagen</th>
                    </tr>
                    <xsl:for-each select="alumno">
                        <xsl:sort select="apellidos" data-type="text" order="ascending"/>
                        <tr>
                            <th><xsl:value-of select="nombre"/></th>
                            <th><xsl:value-of select="apellidos"/></th>
                            <th><xsl:value-of select="telefono"/></th>
                            <th><xsl:value-of select="@repite"/></th>
                            <xsl:apply-templates select="notas"/>
                            <th><img src="{concat('imagenes/',apellidos)}.png" width="100" height="100"/></th>                           
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="notas">
        <th><xsl:value-of select="practicas"/></th>
        <th><xsl:value-of select="examen"/></th>
        <xsl:choose>
            <xsl:when test="((practicas + examen)div 2) &lt; 5">
                <th style="background-color:red"><xsl:value-of select="(practicas + examen)div 2"/></th>    
            </xsl:when>
            <xsl:when test="((practicas + examen)div 2) &gt;= 8">
                <th style="background-color:blue"><xsl:value-of select="(practicas + examen)div 2"/></th>    
            </xsl:when>
            <xsl:otherwise>
                <th><xsl:value-of select="(practicas + examen)div 2"/></th>    
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>