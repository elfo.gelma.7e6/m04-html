<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:variable name="muse" select="discos/group[@id='museId']/name"/>
    <xsl:variable name="feeder" select="/discos/group[@id='feederId']/name"/>
    <xsl:template match="/">
        <xsl:element name="lista">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="discos">
        <xsl:for-each select="disco">
            <xsl:element name="disco">
                    <xsl:value-of select="title"/>
                    es interpretado por: 
                    <xsl:choose>
                        <xsl:when test="interpreter/@id = 'museId'">
                            <xsl:value-of select="$muse"/>
                        </xsl:when>
                        <xsl:when test="interpreter/@id = 'feederId'">
                            <xsl:value-of select="$feeder"/>
                        </xsl:when>
                    </xsl:choose>

            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>