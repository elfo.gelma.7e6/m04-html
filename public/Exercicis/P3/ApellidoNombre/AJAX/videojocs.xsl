<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/Videojuegos">
        <xsl:apply-templates select="Videojuego"/>
    </xsl:template>
    <xsl:template match="Videojuego">

        <xsl:element name="article">
            <xsl:element name="img">
                <xsl:attribute name="src"><xsl:value-of select="InfoGeneral/Foto"/></xsl:attribute>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="class">information</xsl:attribute>
                <xsl:element name="h3">
                    <xsl:value-of select="Nombre"/>
                </xsl:element>
                <xsl:element name="p">
                    <xsl:value-of select="InfoGeneral/Descripcion"/>
                </xsl:element>
            </xsl:element>
            <xsl:element name="div">
                <xsl:attribute name="class">position-final</xsl:attribute>
                <xsl:element name="div">
                    <xsl:attribute name="class">button</xsl:attribute>
                    <xsl:element name="a">
                            <xsl:attribute name="href">detall.html?id=<xsl:value-of select="@idVideojuego"/></xsl:attribute>más info</xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>