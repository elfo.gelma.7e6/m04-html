<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/Videojuegos">
        <xsl:apply-templates select="Videojuego"/>
    </xsl:template>
    <xsl:template match="Videojuego">
        <xsl:element name="article">
            <xsl:element name="div">
                <xsl:attribute name="class">info</xsl:attribute>
                <xsl:element name="h2">
                    <xsl:value-of select="Nombre"/>
                </xsl:element>
                <xsl:element name="ul">
                    <xsl:element name="li">
                        <xsl:element name="span">
                            Idiomas
                        </xsl:element>
                        <xsl:value-of select="InfoGeneral/Idiomas"/>
                    </xsl:element>
                    <xsl:element name="li">
                        <xsl:element name="span">
                            Desarrollador:
                        </xsl:element>
                        <xsl:value-of select="InfoGeneral/Desarrollador"/>
                    </xsl:element>
                    <xsl:element name="li">
                        <xsl:element name="span">
                            Distribuidor
                        </xsl:element>
                        <xsl:value-of select="InfoGeneral/Distribuidor"/>
                    </xsl:element>
                    <xsl:element name="li">
                        <xsl:element name="span">
                            Descripcion
                        </xsl:element>
                        <xsl:value-of select="InfoGeneral/Descripcion"/>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="h3">
                    Plataformas:
                </xsl:element>
                <xsl:apply-templates select="Plataformas"/>
                <xsl:element name="h3">
                    Modos de Juego
                </xsl:element>
                <xsl:apply-templates select="ModosJuego"/>
                
            </xsl:element>
            <xsl:element name="img">
                <xsl:attribute name="src">
                    <xsl:value-of select="InfoGeneral/Foto"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    <xsl:template match="Plataformas">
        <xsl:for-each select="Plataforma">
            <xsl:element name="p">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="ModosJuego">
        <xsl:for-each select="ModoJuego">
            <xsl:element name="p">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>